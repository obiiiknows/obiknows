import Vue from 'vue'
import Router from 'vue-router'

// Pages
import Home from '@/pages/Home'
import Entrance from '@/pages/Entrance'
import About from '@/pages/About'
import Clouds from '@/pages/Clouds'
import Blog from '@/pages/Blog'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Entrance', component: Entrance },
    { path: '/home', name: 'Home', component: Home },
    { path: '/about', name: 'About', component: About },
    { path: '/company', name: 'Company', component: Home },
    { path: '/blog', name: 'Blog', component: Blog },
    { path: '/clouds', name: 'Clouds', component: Clouds }
  ]
})
